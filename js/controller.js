angular.module('moduleGuestbook', ['ngRoute'])

        //User Service to remember if the user is logged in.
        .factory('UserService', [

        function () {

            var status = {
                isLoggedIn: false,
                username: ''
            }
            return {
                get: function () {
                    return status;
                },
                set: function (currentUser, currentID) {
                    status.username = currentUser;
                    status.ID = currentID;
                    status.isLoggedIn = true;
                    return status;
                },
                reset: function () {
                    status = {
                        isLoggedIn: false,
                        username: ''
                    }
                    return status;
                }
            };
        }])

        //Page Service to change each pages H1 tag.
        .factory('PageService', [

        function () {

            var page = {
                currentPage: ''
            }
            return {
                get: function () {
                    return page;
                },
                set: function (pageName) {
                    page.currentPage = pageName;
                    return page;
                }
            };
         }])

    //The template URL routing.
    .config(['$routeProvider',
            function ($routeProvider) {
                $routeProvider.when('/guestbook', {
                    templateUrl: 'partials/guestbook.html',
                    controller: 'controllerGuestbook as ctrl'
                })
                .when('/login', {
                    templateUrl: 'partials/login.html',
                    controller: 'controllerLogin as ctrl'
                })
                .when('/logout', {
                    templateUrl: 'partials/logout.html',
                    controller: 'controllerLogout as ctrl'
                })
                .when('/register', {
                    templateUrl: 'partials/registration.html',
                    controller: 'controllerRegistration as ctrl'
                })
                .when('/users', {
                    templateUrl: 'partials/users.html',
                    controller: 'controllerUsers as ctrl'
                })
                .when('/user/:userID', {
                    templateUrl: 'partials/profile.html',
                    controller: 'controllerProfile as ctrl'
                })
                .otherwise({
                    redirectTo: '/guestbook'
                })
     }])

    //The default controller, binding the two services to the view.
    .controller('controllerMain', ['UserService', 'PageService',
    function (UserService, PageService) {
        var	self = this;
        self.userService = UserService;
        self.pageService = PageService;
    }])



    //Guestbook Controller

    .controller('controllerGuestbook', ['$http', 'UserService', 'PageService',
                                                                                               
    function ($http, UserService, PageService) {
        var	self = this;
        var url;
        self.userService = UserService;
        
        //Setting the page name.
        PageService.set("Guestbook");
        
        //Display the comments.
        displayComments();
        
        self.submit = function () {
            if (UserService.get().isLoggedIn) {
                url = "https://myangularjsguestbook.couchappy.com/comments";
                
                //Getting the current date.
                var d = new Date();
                var current_date = d.getDate();
                var current_month = d.getMonth() + 1;
                var current_year = d.getFullYear();
                var current_time = d.getTime();
                var current_hour = d.getHours();
                var current_minutes = d.getMinutes();
                
                //Formatting the date and time.
                var date = current_date + " " + current_month + " " + current_year;
                var time = current_hour + ":" + current_minutes;
                
                //Storing the user inputs into an object.
                var commentObject = {username: UserService.get().username, comment: self.content, date: date, time: time};
                
                //Initiate POST into a new CouchDB comment document.
                $http({
                    url: url,
                    method: 'POST',
                    contentType: "application/json",
                    data: commentObject,
                    withCredentials: true,
                    headers: {'Authorization': auth_hash("testadmin", "testadmin")}
                    })
                        
                    //Success! Comment is now stored in a document in CouchDB.
                    .success(function(data, status, headers, config) {
                        self.response = true;
                        self.responseText = "Comment successfully posted!";
                        displayComments();
                    })
                        
                    //Error posting the comment data,
                    .error(function(data, status, headers, config) {
                        console.log(headers);
                        console.log(config);
                        self.response = true;
                        self.responseText = "There was a problem posting your comment. Please try again.";
                    });
              }
        }
        
        self.delete = function (id, rev) {
            
            //Delete the selected comment.
            url = "https://myangularjsguestbook.couchappy.com/comments/" + id + "?rev=" + rev;
            $http({url: url, method: 'DELETE'})
            
                //Success! The comment has now been deleted.
                .success(function(data, status, headers, config) {
                    displayComments();
                })
            
                //Error deleting the current comment.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.response = true;
                    self.responseText = "There was a problem deleting your comment. Please try again.";
                });
        }
        
        self.enableEditing = function(id, comment) {
            //Enabling editing state of the current comment.
            self.editingID = id;
            self.editedComment = comment;
        }
        
        self.makeChanges = function(id, rev) {
            url = "https://myangularjsguestbook.couchappy.com/comments/" + id + "?rev=" + rev;
                
                //Getting the current date.
                var d = new Date();
                var current_date = d.getDate();
                var current_month = d.getMonth() + 1;
                var current_year = d.getFullYear();
                var current_time = d.getTime();
                var current_hour = d.getHours();
                var current_minutes = d.getMinutes();
                
                //Formatting the date and time.
                var date = current_date + " " + current_month + " " + current_year;
                var time = current_hour + ":" + current_minutes;
                
                //Storing the edited user inputs into an object.
                var editedCommentObject = {username: UserService.get().username, comment: self.editedComment, date: date, time: time};
            
                //Replace the current CouchDB document with our new object.
                $http({
                    url: url,
                    method: 'PUT',
                    contentType: "application/json",
                    data: editedCommentObject,
                    withCredentials: true,
                    headers: {'Authorization': auth_hash("testadmin", "testadmin")}
                    })
                                    
                    //Success! The comment has been edited. Now, to refresh the comments and get out of editing state.
                    .success(function(data, status, headers, config) {
                        displayComments();
                        self.editingID = '';
                        self.editedComment = '';
                    })
                                    
                    //Error putting the edited user's document.
                    .error(function(data, status, headers, config) {
                        console.log(headers);
                        console.log(config);
                        self.response = true;
                        self.responseText = "There was a problem editing your comment. Please try again.";
                    });
            
        }
        
        function auth_hash(username, password) {
            //Creating an authorization hash for the POST and PUT to CouchDB.
            return 'Basic '+btoa(username+':'+password);
        }
        
        function displayComments() {
            url = "https://myangularjsguestbook.couchappy.com/comments/_design/comments/_view/comments";
            
            //Get all the comments in the database.
            $http({url: url, method: 'GET'})
            
                //No problems, so add them all to a variable.
                .success(function(data, status, headers, config) {
                    self.comments = data.rows;
            })
                //There's been a problem. So, let's inform the user.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.response = true;
                    self.responseText = "There was a problem getting the comments. Please try again.";
            });
        }
    }])



    //Login Controller

    .controller('controllerLogin', ['$http', 'UserService', 'PageService',
    function ($http, UserService, PageService) {
        var	self = this;
        
        //Setting the page name.
        PageService.set("Login");
        
        self.login = function () {
            var hashedPassword = CryptoJS.SHA1(self.password).toString();
            var url = "https://myangularjsguestbook.couchappy.com/users/_design/users/_view/loginCheck?key=%5B%22" + self.username + "%22%2C%22" + hashedPassword + "%22%5D";
            
                //GET to CouchDB username and password design document to check if the username and password are correct.
                $http({url: url, method: 'GET'})
                .success(function(data, status, headers, config) {
                    
                    //Username and password found a match!
                    if (data.rows.length === 1) {
                        self.response = true;
                        self.responseText = "You have been successfully logged in!";
                        UserService.set(data.rows[0].key[0], data.rows[0].id);
                    
                    //Username and password found no match.
                    } else {
                        self.response = true;
                        self.responseText = "Incorrect username and/or password.";
                    }
                })
                
                //Error getting the data from CouchDB.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.response = true;
                    self.responseText = "There was a problem logging you in. Please try again.";
                });
            
        };
        
    }])



    //Logout Controller.

    .controller('controllerLogout', ['$http', 'UserService', 'PageService', 
    function ($http, UserService, PageService) {
        "use strict";
        var	self = this;
        
        //Setting the page name.
        PageService.set("Logout");
        
        //Reset the user service.
        UserService.reset();
        
        //Display logged out confirmation message.
        self.response = true;
        self.responseText = "You have been successfully logged out!";
        
    }])



    //Registration Controller.

    .controller('controllerRegistration', ['$http', 'PageService',
    function ($http, PageService) {
        var	self = this;
        
        //Setting the page name.
        PageService.set("Registration Form");
        
        self.submit = function () {
            var url;
            
            //Getting the current date.
            var d = new Date();
            var current_date = d.getDate();
            var current_month = d.getMonth() + 1;
            var current_year = d.getFullYear();
                
            //Formatting the date and time.
            var date = current_date + " " + current_month + " " + current_year;
            
            //Storing the user inputs into an object.
            var hashedPassword = CryptoJS.SHA1(self.password).toString();
            var userObject = {username: self.username, password: hashedPassword, firstname: self.firstname, lastname: self.lastname, age: self.age, email: self.email, 
                              title: "Member", registrationdate: date};
            
            //Check if all the validation checks are correct.
            if (self.registration.$valid) {
                url = "https://myangularjsguestbook.couchappy.com/users/_design/users/_view/username?startkey=%22" + self.username + "%22&endkey=%22" + self.username + "%22";
                
                //GET to CouchDB username design document to check if the username already exists.
                $http({url: url, method: 'GET'})
                .success(function(data, status, headers, config) {
                    
                    //Found a document with that username.
                    if (data.rows.length === 1) {
                        self.response = true;
                        self.responseText = "That username has already been taken.";
                    }
                    
                    //No username found.
                    else {
                        url = "https://myangularjsguestbook.couchappy.com/users";
                        
                        //Initiate POST to enter user details in CouchDB.
                        $http({
                            url: url,
                            method: 'POST',
                            contentType: "application/json",
                            data: userObject,
                            withCredentials: true,
                            headers: {'Authorization': auth_hash("testadmin", "testadmin")}
                        })
                        
                        //Success! Account is now stored as a document in CouchDB.
                        .success(function(data, status, headers, config) {
                            self.response = true;
                            self.responseText = "Account successfully created.";
                        })
                        
                        //Error posting the data to CouchDB.
                        .error(function(data, status, headers, config) {
                            console.log(headers);
                            console.log(config);
                            self.response = true;
                            self.responseText = "There was a problem with the registration process. Please try again.";
                        });
                    } 
                })
                
                //Error getting the data from CouchDB.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.response = true;
                    self.responseText = "There was a problem with the registration process. Please try again.";
                });
                
            //Some form validation checks are not valid.
            } else {
                self.response = true;
                self.responseText = "There are some errors with your details. Please correct them and try again.";
            }
        };
        
        //Creating an authorization hash for the POST to CouchDB.
        function auth_hash(username, password) {
            return 'Basic '+btoa(username+':'+password);
        }
        
    }])



    //User List Controller.

    .controller('controllerUsers', ['$http', 'PageService', 
    function ($http, PageService) {
        var self = this;
        
        //Setting the page name.
        PageService.set("User List");
        
        //GET a list of all the users.
        var url = "https://myangularjsguestbook.couchappy.com/users/_design/users/_view/userlist";
            $http({url: url, method: 'GET'})
            
                //Success. So, let's display the list to the user.
                .success(function(data, status, headers, config) {
                    self.users = data.rows;
            })
                //Error getting the list of users.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.error = true;
                    self.errorText = "There's been a problem getting the user list. Please try again.";
            });
     }])



    //User Profile Controller.
    
    .controller('controllerProfile', ['$http', '$routeParams', 'UserService', 'PageService',
    function ($http, $routeParams, UserService, PageService) {
        var self = this;
        var url;
        self.userService = UserService;
        
        //Get the user's data.
        getUserData();
        
        self.edit = function () {
            
            //If the edited profile form is valid.
            if (self.profile.$valid) {
                
                //If it's our first time editing, get the stored rev ID.
                if (!self.edited) {
                    self.currentRev = self.userInfo._rev;
                }
                
                url = "https://myangularjsguestbook.couchappy.com/users/" + self.userInfo._id + "?rev=" + self.currentRev;
                var hashedPassword = CryptoJS.SHA1(self.userInfo.newPassword).toString();
                var editedUserObject = {username: self.userInfo.username, password: hashedPassword, firstname: self.userInfo.firstname, lastname: self.userInfo.lastname, age: self.userInfo.age, 
                                        email: self.userInfo.email, title: self.userInfo.title, posts: self.postCount, registrationdate: self.userInfo.registrationdate};
            
                //Replace the current CouchDB user document with our edited user information.
                    $http({
                        url: url,
                        method: 'PUT',
                        contentType: "application/json",
                        data: editedUserObject,
                        withCredentials: true,
                        headers: {'Authorization': auth_hash("testadmin", "testadmin")}
                        })
                                    
                        //Success! The user's details have been updated. Now, to update the controller with the new rev ID and to get out of editing state.
                        .success(function(data, status, headers, config) {
                        
                            self.edited = true;
                            self.currentRev = data.rev;

                            self.editingMode = false;
                        })
                                    
                        //Error putting the edited user's document.
                        .error(function(data, status, headers, config) {
                            console.log(headers);
                            console.log(config);
                            self.error = true;
                            self.errorText = "There was a problem submitting your details. Please try again.";
                        });
                
            //Some form validation checks are not valid.
            } else {
                self.error = true;
                self.errorText = "There are some errors with your details. Please correct them and try again.";
            }
        }
        
        self.viewPosts = function () {
            url = "https://myangularjsguestbook.couchappy.com/comments/_design/comments/_view/comments?startkey=%22" + self.userInfo.username + "%22&endkey=%22" + self.userInfo.username + "%22";
            
            //GET the list of posts for that user.
            $http({url: url, method: 'GET'})
                
                //Success. So, let's display the list of posts to the user.
                .success(function(data, status, headers, config) {
                    self.posts = data.rows;
                })
            
                //Error getting the posts.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.error = true;
                    self.errorText = "There's been a problem getting a list of your posts. Please try again.";
                });
        }
        
        function getUserData() {
            
            //Getting the ID of the user being viewed from the route parameter.
            var userID = $routeParams.userID;
            
            //GET the user's details.
            url = "https://myangularjsguestbook.couchappy.com/users/_design/users/_view/all?startkey=%22" + userID + "%22&endkey=%22" + userID + "%22";
            $http({url: url, method: 'GET'})
            
                //Successful GET. So, add the results to the page.
                .success(function(data, status, headers, config) {
                    self.userInfo = data.rows[0].value;
                
                    //Setting the page name.
                    PageService.set(self.userInfo.username + "'s profile");
                
                    //Now to do a second GET on a reduce function to retrieve the users post count.
                    url = "https://myangularjsguestbook.couchappy.com/comments/_design/comments/_view/postcount?group_level=1&startkey=%22" + self.userInfo.username + "%22&endkey=%22" + self.userInfo.username + "%22";
                        $http({url: url, method: 'GET'})
                        
                        //Successful GET.
                        .success(function(data, status, headers, config) {
                            
                            //If the user hasn't posted anything yet, change post count to 0.
                            if (data.rows.length === 0) {
                                self.postCount = 0;
                                
                            //The user has posted something, so use the post count from the GET.
                            } else {
                                self.postCount = data.rows[0].value;
                            }
                        })
                        
                        //Error getting the post count.
                        .error(function(data, status, headers, config) {
                            console.log(headers);
                            console.log(config);
                            self.error = true;
                            self.errorText = "There was a problem calculating the users post count. Please try again.";
                        });
                })
            
                //Error getting the users details.
                .error(function(data, status, headers, config) {
                    console.log(headers);
                    console.log(config);
                    self.error = true;
                    self.errorText = "There was a problem getting the users details. Please try again.";
                });
        }
        
        //Creating an authorization hash for the POST to CouchDB.
        function auth_hash(username, password) {
            return 'Basic '+btoa(username+':'+password);
        }
    }]);