{
    "language": "javascript",
    "views": {
        "comments": {
            "map": "function(doc) { emit(doc.username, doc) }"
        },
        "postcount": {
            "map": "function(doc) { emit(doc.username, doc.comment) }",
            "reduce": "_count"
        }
    },
    "shows": {
        "comments": "function(doc, req) { return '<h1>' + doc.username + '</h1>' }"
    }
}