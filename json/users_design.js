{
    "language": "javascript",
    "views": {
        "loginCheck": {
            "map": "function(doc) { emit([doc.username, doc.password], null) }"
        },
        "username": {
            "map": "function(doc) { emit(doc.username, null) }"
        },
        "userlist": {
            "map": "function(doc) { emit(doc._rev, {username: doc.username, title: doc.title, registrationdate: doc.registrationdate}) }"
        },
        "all": {
            "map": "function(doc) { emit(doc._id, doc) }"
        }
    },
    "shows": {
        "username": "function(doc, req) { return '<table>' + doc.username + '</table>' }"
    }
}